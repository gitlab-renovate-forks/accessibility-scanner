<h1 align="center">Welcome to the <code>Accessibility</code> Scanner 👋</h1>
<p>
  <a href="https://gitlab.com/gitlab-org/frontend/playground/accessibility-scanner" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://gitlab.com/gitlab-org/frontend/playground/accessibility-scanner/-/blob/main/LICENSE" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

## Note

This is a copy of the
[Pajamas Adoption Scanner](https://gitlab.com/gitlab-org/frontend/pajamas-adoption-scanner),
but with rules and UI changes more suited for tracking progress of the Accessibility testing

## Motivation

We don't want to write ESLint rules for these things (yet), and we also get to
see progress over time!

## Prerequisites

1. Install [Semgrep][semgrep].

## Usage

Scan GitLab
You can use Semgrep with the latest rules, which are published via GitLab pages.

```sh
# inside the GitLab folder
semgrep -c https://gitlab-org.gitlab.io/frontend/playground/vue-compat-scanner/vcs-rules.yml {ee/,}spec
```

### Create issues for all warnings

Combine Semgrep with [jq](https://stedolan.github.io/jq/) and the [Batch Issue Creator](https://gitlab.com/gitlab-org/frontend/playground/batch-issue-creator) to create an issue for each warning.

## Developing locally

Follow the [prerequisites](#prerequisites), then:

1. Clone this repository
2. Symlink an existing local copy of the GitLab repository to `gitlab`
   ```sh
   ln -s <path_to_gitlab> gitlab
   ```

### Dashboard

The dashboard needs to have scan data available to work. To create that data,
either download the latest scans from CI (this is much faster), or generate the
scan(s) locally.

#### Download scans from CI

Run:

```sh
bin/download_latest_scans.sh
```

#### Generate scans locally

Run:

```sh
bin/scan_gitlab_json.sh HEAD
NUM_WEEKS_TO_SCAN=2 bin/scan_gitlab_history_json.sh
bin/process_scans.js
bin/copy_scans_to_dashboard.sh
```

This will run a scan against the currently checked-out HEAD of the local GitLab
repository you symlinked (or cloned) in [prerequisites](#prerequisites), and
scan two historical commits. Any valid ref can be used instead of HEAD, but be
aware that the script will try to check that ref out in order to run the scan.

#### Start a development server

With the scans prepared, you can run the development server:

```sh
cd dashboard
yarn install
yarn dev
```

### Run tests

```sh
bin/test.sh
```

## Author

👥 **[GitLab Product Accessibility Working Group](https://about.gitlab.com/company/team/structure/working-groups/product-accessibility/)**

## Contributing

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/gitlab-org/frontend/playground/accessibility-scanner/-/issues).

- If you want to contribute new rules, have a look at the readme [rules](./rules)

[semgrep]: https://semgrep.dev
