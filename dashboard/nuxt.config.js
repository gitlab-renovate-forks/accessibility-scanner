const base = process.env.CI_PAGES_URL ? new URL(process.env.CI_PAGES_URL).pathname : '/';

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Accessibility scanner dasbhboard',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/svg+xml', href: 'favicon.svg' },
      {
        rel: 'icon',
        type: 'image/png',
        href: 'favicon.png',
        sizes: '48x48',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/stylesheets/main.css',
    '~/assets/stylesheets/fonts.css',
    '~/../node_modules/@gitlab/ui/dist/index.css',
    '~/assets/stylesheets/tailwind.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  router: {
    base,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['@gitlab/ui', 'shared'],
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  },
};
