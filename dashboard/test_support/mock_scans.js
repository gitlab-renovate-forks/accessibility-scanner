const toFullId = (id) => `vcs.rules.${id}`;
export const makeResult = ({
  severity = 'ERROR',
  id = 'missing-axe-feature-test',
  path = 'some/path.rb',
  accessibilityCompliant = false,
  group = 'group::foundations',
} = {}) => ({
  check_id: toFullId(id),
  end: {
    col: 22,
    line: 46,
    offset: 2542,
  },
  extra: {
    lines: 'some code',
    message: 'some message',
    metadata: { accessibilityCompliant },
    severity,
  },
  path,
  start: {
    col: 14,
    line: 46,
    offset: 2534,
  },
});

const makeRule = ({
  id,
  severity = 'ERROR',
  message = `message for ${id}`,
  accessibilityCompliant = false,
} = {}) => ({
  id: toFullId(id),
  message,
  severity,
  metadata: {
    accessibilityCompliant,
    category: 'best-practice',
    reference:
      'https://docs.gitlab.com/ee/development/fe_guide/accessibility.html#separate-file-for-extensive-test-suites',
  },
});

export const invalidScans = [undefined, null, {}, { results: [{}] }];

export const getEmptyScan = () => ({ results: [] });

export const getMinimalScan = () => ({ results: [makeResult()] });

export const getTypicalScan = () => ({
  results: [
    {},
    { path: 'different/file_type.rb' },
    { id: 'axe-clean-feature-test', severity: 'INFO', accessibilityCompliant: true },
    { id: 'missing-axe-feature-suite' },
    { group: 'group::code review', path: 'mr/mr.rb' },
    { id: 'skipping-axe-test', severity: 'WARNING' },
  ].map(makeResult),
});

export const getHistory = () => [
  {
    date: '2021-01-01',
    summary: {
      'missing-axe-feature-suite': 1,
      'missing-axe-feature-test': 1,
      'axe-clean-feature-test': 1,
      'skipping-axe-test': 1,
    },
  },
  {
    date: '2022-02-02',
    summary: {
      'missing-axe-feature-suite': 2,
      'missing-axe-feature-test': 2,
      'axe-clean-feature-test': 1,
      'skipping-axe-test': 1,
    },
  },
];

export const getScanSummary = () => ({
  current: {
    file: 'current_2022-10-27_abc123.json',
  },
  history: getHistory(),
});

export const getRules = () =>
  [
    { id: 'missing-axe-feature-test' },
    { id: 'axe-clean-feature-test', severity: 'INFO' },
    { id: 'missing-axe-feature-suite' },
    {
      id: 'axe-clean-feature-suite',
      severity: 'INFO',
    },
    {
      id: 'skipping-axe-test',
      severity: 'WARNING',
    },
  ].reduce((acc, rule) => {
    acc.set(rule.id, makeRule(rule));
    return acc;
  }, new Map());
