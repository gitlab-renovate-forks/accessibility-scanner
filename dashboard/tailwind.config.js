const tailwindDefaults = require('@gitlab/ui/tailwind.defaults');

/** @type {import('tailwindcss').Config} */
module.exports = {
  presets: [tailwindDefaults],
  content: [
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './components/**/*.vue',
    '../node_modules/@gitlab/ui/dist/**/*.{vue,js}',
  ],
};
