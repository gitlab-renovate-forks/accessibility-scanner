import { shortId } from 'shared/rules.js';

const filteredHistoryToStackedColumnChartProps = (history, rules, rulePredicate) =>
  history.reduce(
    (acc, scan, i) => {
      acc.groupBy[i] = scan.date;

      Object.entries(scan.summary).forEach(([id, count]) => {
        const rule = rules.get(id);
        if (!rule) {
          // This can happen if there's a mismatch between the scan results and
          // the combined rules file. This *should* only be possible in
          // development if one was updated without the other.
          // eslint-disable-next-line no-console
          console.warn(`Invalid history: non-existent rule with id "${id}"`);
          return;
        }

        if (!rulePredicate(rule)) return;

        const name = shortId(id);
        let bar = acc.bars.find((bar) => bar.name === name);
        if (!bar) {
          bar = { name, data: Array(i + 1).fill(0) };
          acc.bars.push(bar);
        }

        bar.data[i] = (bar.data[i] ?? 0) + count;
      });

      return acc;
    },
    {
      bars: [],
      groupBy: [],
      xAxisType: 'category',
      xAxisTitle: 'Date',
      yAxisTitle: 'Findings',
    },
  );

/**
 * Transforms a history array to props for the GlStackedColumnChart component.
 *
 * @param {Array} history Array summarizing multiple semgrep scans.
 * @param {Map} rules Array of rule definitions.
 * @returns {Object}
 */
export const historyToStackedColumnChartProps = (history, rules) =>
  filteredHistoryToStackedColumnChartProps(history, rules, () => true);
