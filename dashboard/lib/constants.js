import { colorPaletteDefault } from '@gitlab/ui/src/utils/charts/theme';

export const GITLAB_REPO_WEB_URL = 'https://gitlab.com/gitlab-org/gitlab';
export const [, COLOR_WARNING, , COLOR_INFO, COLOR_ERROR] = colorPaletteDefault;
