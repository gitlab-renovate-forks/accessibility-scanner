const getCompliancePercentageFor = (item) =>
  `${
    item.totalFindings ? +(100 * (item.compliantFindings / item.totalFindings)).toFixed(2) : 100
  }%`;

const getFindingsByRuleIdMap = (scan) =>
  scan.results.reduce((acc, finding) => {
    if (!acc.has(finding.check_id)) acc.set(finding.check_id, 0);
    acc.set(finding.check_id, acc.get(finding.check_id) + 1);
    return acc;
  }, new Map());

/**
 * Return the rules-by-id table data props (fields, items, primaryKey) for the
 * given scan and rules
 *
 * @param {Object} The JSON output of a semgrep scan.
 * @param {Promise<Map<string, Rule>>} A map of rule ids to rule definitions.
 * @returns {Object} The fields, items and primaryKey props for a table.
 */
export const allRulesTableDataProps = (scan, rules) => {
  const fields = [
    { key: 'id', label: 'Rule ID', sortable: false },
    { key: 'severity', label: 'Severity', sortable: true },
    { key: 'message', label: 'Message', sortable: false },
    { key: 'findings', label: 'Findings', sortable: true },
  ];

  const findingsByRuleId = getFindingsByRuleIdMap(scan);

  const items = [...rules.values()].map((rule) => {
    return {
      id: rule.id,
      message: rule.message,
      severity: rule.severity,
      findings: findingsByRuleId.get(rule.id) ?? 0,
    };
  });

  return { primaryKey: fields[0].key, fields, items };
};

/**
 * Return the rules-by-group table data props (fields, items, primaryKey)
 * for the given scan and rules.
 *
 * Though, this table is more useful as a table about findings than rules,
 * since rules cannot be group-specific, unlike component labels.
 *
 * @param {Object} The JSON output of a semgrep scan.
 * @param {Promise<Map<string, Rule>>} A map of rule ids to rule definitions.
 * @returns {Object} The fields, items and primaryKey props for a table.
 */
export const groupRulesTableDataProps = (scan, rules) => {
  const fields = [
    { key: 'groupLabel', label: 'Group label', sortable: true },
    { key: 'defiantRules', label: 'Non-compliant rules', sortable: true },
    { key: 'defiantFindings', label: 'Non-compliant findings', sortable: true },
    { key: 'exceptionFindings', label: 'Findings with exceptions', sortable: true },
    { key: 'compliantRules', label: 'Compliant rules', sortable: true },
    { key: 'compliantFindings', label: 'Compliant findings', sortable: true },
    { key: 'totalFindings', label: 'Total findings', sortable: true },
    { key: 'compliancePercentage', label: 'Compliance percentage', sortable: true },
  ];

  const defiantRules = new Set();
  const compliantRules = new Set();

  const itemsMap = [...scan.results].reduce((acc, finding) => {
    const { group } = finding.extra;
    if (!acc.has(group))
      acc.set(group, {
        group,
        defiantRules: 0,
        defiantFindings: 0,
        exceptionFindings: 0,
        compliantRules: 0,
        compliantFindings: 0,
        totalFindings: 0,
      });

    const item = acc.get(group);

    if (finding.extra.metadata.accessibilityCompliant) {
      item.compliantFindings += 1;
      compliantRules.add(finding.check_id);
    } else {
      if (finding.check_id === 'vcs.rules.skipping-axe-test') {
        item.exceptionFindings += 1;
      } else {
        item.defiantFindings += 1;
      }
      defiantRules.add(finding.check_id);
    }

    item.defiantRules = defiantRules.size;
    item.compliantRules = compliantRules.size;
    item.totalFindings = item.defiantFindings + item.exceptionFindings + item.compliantFindings;
    item.compliancePercentage = getCompliancePercentageFor(item);

    return acc;
  }, new Map());

  const items = Array.from(itemsMap.values()).map((item) => ({
    ...item,
  }));

  return { primaryKey: fields[0].key, fields, items };
};
