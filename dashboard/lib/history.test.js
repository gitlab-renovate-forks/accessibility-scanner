import { historyToStackedColumnChartProps } from './history';
import { getHistory, getRules } from '~/test_support/mock_scans';

describe('historyToStackedColumnChartProps', () => {
  it('returns correct props for unfiltered history', () => {
    expect(historyToStackedColumnChartProps(getHistory(), getRules())).toEqual({
      groupBy: ['2021-01-01', '2022-02-02'],
      bars: [
        {
          name: 'missing-axe-feature-suite',
          data: [1, 2],
        },
        {
          name: 'missing-axe-feature-test',
          data: [1, 2],
        },
        {
          name: 'axe-clean-feature-test',
          data: [1, 1],
        },
        {
          name: 'skipping-axe-test',
          data: [1, 1],
        },
      ],
      xAxisType: 'category',
      xAxisTitle: 'Date',
      yAxisTitle: 'Findings',
    });
  });
});
