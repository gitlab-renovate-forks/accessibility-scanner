import { allRulesTableDataProps, groupRulesTableDataProps } from './rules_table';
import { getTypicalScan, getRules } from '~/test_support/mock_scans';

describe('allRulesTableDataProps', () => {
  it('returns correct props for typical scan', () => {
    expect(allRulesTableDataProps(getTypicalScan(), getRules())).toEqual({
      primaryKey: 'id',
      fields: [
        { key: 'id', label: 'Rule ID', sortable: false },
        { key: 'severity', label: 'Severity', sortable: true },
        { key: 'message', label: 'Message', sortable: false },
        { key: 'findings', label: 'Findings', sortable: true },
      ],
      items: [
        {
          id: 'vcs.rules.missing-axe-feature-test',
          findings: 3,
          message: 'message for missing-axe-feature-test',
          severity: 'ERROR',
        },
        {
          id: 'vcs.rules.axe-clean-feature-test',
          findings: 1,
          message: 'message for axe-clean-feature-test',
          severity: 'INFO',
        },
        {
          id: 'vcs.rules.missing-axe-feature-suite',
          findings: 1,
          message: 'message for missing-axe-feature-suite',
          severity: 'ERROR',
        },
        {
          id: 'vcs.rules.axe-clean-feature-suite',
          findings: 0,
          message: 'message for axe-clean-feature-suite',
          severity: 'INFO',
        },
        {
          id: 'vcs.rules.skipping-axe-test',
          findings: 1,
          message: 'message for skipping-axe-test',
          severity: 'WARNING',
        },
      ],
    });
  });
});

describe('groupRulesTableDataProps', () => {
  it('returns correct props for typical scan', () => {
    expect(groupRulesTableDataProps(getTypicalScan(), getRules())).toMatchObject({
      primaryKey: 'groupLabel',
      fields: [
        { key: 'groupLabel', label: 'Group label', sortable: true },
        { key: 'defiantRules', label: 'Non-compliant rules', sortable: true },
        { key: 'defiantFindings', label: 'Non-compliant findings', sortable: true },
        { key: 'exceptionFindings', label: 'Findings with exceptions', sortable: true },
        { key: 'compliantRules', label: 'Compliant rules', sortable: true },
        { key: 'compliantFindings', label: 'Compliant findings', sortable: true },
        { key: 'totalFindings', label: 'Total findings', sortable: true },
        { key: 'compliancePercentage', label: 'Compliance percentage', sortable: true },
      ],
      items: [
        {
          defiantRules: 3,
          defiantFindings: 4,
          exceptionFindings: 1,
          compliantRules: 1,
          compliantFindings: 1,
          totalFindings: 6,
          compliancePercentage: '16.67%',
        },
      ],
    });
  });
});
