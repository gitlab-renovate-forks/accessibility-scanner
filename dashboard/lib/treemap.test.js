import { scanToTreeMapSeries, defaultOptions, findingsForNode } from './treemap';
import { enhanceScan } from './api';
import {
  invalidScans,
  getEmptyScan,
  getMinimalScan,
  getTypicalScan,
  makeResult,
} from '~/test_support/mock_scans';
import {
  groupByOptions,
  GROUP_BY_DIRECTORY,
  GROUP_BY_SEVERITY,
  GROUP_BY_FILE_TYPE,
  GROUP_BY_NOTHING,
  GROUP_BY_GROUP,
} from '~/components/group_by.vue';

const groupBys = groupByOptions.map(({ value }) => value);

describe('scanToTreeMapSeries', () => {
  describe('common behaviours', () => {
    describe.each(groupBys)('given groupBy: %s', (groupBy) => {
      describe.each(invalidScans)('given invalid scan %p', (scan) => {
        it('throws', () => {
          expect(() => scanToTreeMapSeries(scan, { groupBy })).toThrow();
        });
      });

      describe('given an empty scan', () => {
        it('returns an empty data array', () => {
          expect(scanToTreeMapSeries(getEmptyScan(), { groupBy }).data).toEqual([]);
        });
      });

      describe('given valid scan', () => {
        it('sets defaults', () => {
          expect(scanToTreeMapSeries(getMinimalScan(), { groupBy })).toMatchObject(defaultOptions);
        });
      });
    });
  });

  describe.each`
    context          | groupBy               | name             | levels
    ${'directories'} | ${GROUP_BY_DIRECTORY} | ${'Directories'} | ${3}
    ${'group'}       | ${GROUP_BY_GROUP}     | ${'Groups'}      | ${3}
    ${'severity'}    | ${GROUP_BY_SEVERITY}  | ${'Severities'}  | ${3}
    ${'file type'}   | ${GROUP_BY_FILE_TYPE} | ${'File types'}  | ${3}
    ${'nothing'}     | ${GROUP_BY_NOTHING}   | ${'Nothing'}     | ${3}
  `('given $context grouping', ({ groupBy, levels, name }) => {
    it('sets the name and levels correctly', () => {
      const series = scanToTreeMapSeries(getMinimalScan(), { groupBy });

      expect(series.name).toBe(name);
      expect(series.levels.length).toBe(levels);
    });
  });

  it('no grouping returns the correct structure for a typical scan', () => {
    expect(scanToTreeMapSeries(getTypicalScan()).data).toMatchObject([
      {
        name: 'missing-axe-feature-test',
        value: 3,
      },
      {
        name: 'axe-clean-feature-test',
        value: 1,
      },
      {
        name: 'missing-axe-feature-suite',
        value: 1,
      },
      {
        name: 'skipping-axe-test',
        value: 1,
      },
    ]);
  });

  it('directory grouping returns the correct structure for a typical scan', () => {
    const results = [
      makeResult({ path: 'ee/spec/frontend/foo/bar_spec.js' }),
      makeResult({ path: 'spec/frontend/foo/bar_spec.js' }),
    ];

    expect(scanToTreeMapSeries({ results }, { groupBy: GROUP_BY_DIRECTORY }).data).toMatchObject([
      {
        name: 'ee/foo',
        children: [
          {
            name: 'missing-axe-feature-test',
            value: 1,
          },
        ],
      },
      {
        name: '~/foo',
        children: [
          {
            name: 'missing-axe-feature-test',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('severity grouping returns the correct structure for a typical scan', () => {
    expect(
      scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_SEVERITY }).data,
    ).toMatchObject([
      {
        name: 'ERROR',
        children: [
          {
            name: 'missing-axe-feature-test',
            value: 3,
          },
          {
            name: 'missing-axe-feature-suite',
            value: 1,
          },
        ],
      },
      {
        name: 'INFO',
        children: [
          {
            name: 'axe-clean-feature-test',
            value: 1,
          },
        ],
      },
      {
        name: 'WARNING',
        children: [
          {
            name: 'skipping-axe-test',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('file type grouping returns the correct structure for a typical scan', () => {
    expect(
      scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_FILE_TYPE }).data,
    ).toMatchObject([
      {
        name: 'rb',
        children: [
          {
            name: 'missing-axe-feature-test',
            value: 3,
          },
          {
            name: 'axe-clean-feature-test',
            value: 1,
          },
          {
            name: 'missing-axe-feature-suite',
            value: 1,
          },
          {
            name: 'skipping-axe-test',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('group grouping returns the correct structure for a typical scan', () => {
    expect(scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_GROUP }).data).toMatchObject([
      {
        children: [
          {
            name: 'missing-axe-feature-test',
            value: 3,
          },
          {
            name: 'axe-clean-feature-test',
            value: 1,
          },
          {
            name: 'missing-axe-feature-suite',
            value: 1,
          },
          {
            name: 'skipping-axe-test',
            value: 1,
          },
        ],
      },
    ]);
  });
});

describe('findingsForNode', () => {
  it('returns no findings given no scan', () => {
    expect(findingsForNode()).toEqual([]);
  });

  it('returns all findings given no node', () => {
    const scan = enhanceScan(getTypicalScan());

    expect(findingsForNode(scan)).toBe(scan.results);
  });

  it('returns the correct findings given a leaf node', () => {
    const scan = enhanceScan(getTypicalScan());
    const series = scanToTreeMapSeries(scan);
    const [node] = series.data;

    expect(node.name).toBe('missing-axe-feature-test');
    expect(findingsForNode(scan, node)).toMatchObject([
      scan.results[0],
      scan.results[1],
      scan.results[4],
    ]);
  });

  it('returns the correct findings given a group node', () => {
    const scan = enhanceScan(getTypicalScan());
    const series = scanToTreeMapSeries(scan, { groupBy: GROUP_BY_SEVERITY });
    const [node] = series.data;

    expect(node.name).toBe('ERROR');
    expect(findingsForNode(scan, node)).toMatchObject([
      scan.results[0],
      scan.results[1],
      scan.results[3],
      scan.results[4],
    ]);
  });
});
