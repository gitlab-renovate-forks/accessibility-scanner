#!/usr/bin/env bash

# shellcheck source=bin/common.sh
source "$(dirname "${BASH_SOURCE[0]}")"/common.sh

LATEST_ARTIFACTS_URL="https://gitlab.com/api/v4/projects/gitlab-org%2ffrontend%2fplayground%2faccessibility-scanner/jobs/artifacts/main/download?job=pages"
TMP_ARCHIVE="$ROOT_DIR/tmp/archive.zip"
TMP_EXTRACT_DIR=$(mktemp -d)
STATIC_DIR="$ROOT_DIR/dashboard/static"

warn "Downloading latest scans..."
curl \
  --silent \
  --show-error \
  --location \
  --output "$TMP_ARCHIVE" \
  "$LATEST_ARTIFACTS_URL"

warn "Extracting to $STATIC_DIR..."
unzip \
  -q \
  "$TMP_ARCHIVE" \
  "public/current*.json" \
  "public/scans.json" \
  "public/rules.json" \
  "public/history/*.json" \
  -d "$TMP_EXTRACT_DIR"
cp \
  -i \
  -R \
  "$TMP_EXTRACT_DIR"/public/* \
  "$STATIC_DIR"

warn "Done!"
