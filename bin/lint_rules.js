#!/usr/bin/env node

/* eslint-disable no-console */

import { getAllRules } from '../lib/rules.js';
import { lintRules } from '../lib/rule_linters.js';

const rules = await getAllRules();

console.warn(`Linting Accessibility scanner ${rules.length} rules`);

const errors = lintRules(rules);

for (const error of errors) {
  console.error(error);
}

console.warn(`Found ${errors.length} errors`);

process.exitCode = errors.length > 0 ? 1 : 0;
